Pipeline Status [![pipeline status](https://gitlab.com/MuhAfkar/tugas2ppw/badges/master/pipeline.svg)](https://gitlab.com/MuhAfkar/tugas2ppw/commits/master)

Coverage        [![coverage report](https://gitlab.com/MuhAfkar/tugas2ppw/badges/master/coverage.svg)](https://gitlab.com/MuhAfkar/tugas2ppw/commits/master)

# Nama Anggota Kelompok
1. Ahmad Wigo
2. Danang Joewono
3. Reza Akbar
4. Muhammad Afkar

# Link Herokuapp
Link: [https://tugas2sabi.herokuapp.com/](https://tugas2sabi.herokuapp.com/)